#Faila

Are you developing cloud-native, fail-tolerant, stateless service? Really? 
The only way to answer this question - is to adopt [Chaos Engineering](https://principlesofchaos.org/).
Faila is a tool to perform chaos experiments on your cloud-based applications. It's developed with flexibility in mind.

#Getting started

##Choose persistence storage

Depending on how many services/resources/stacks you are running, you could go lightweight, tag-based approach or more heavy-weight setup with DynamoDb.

> We run thousands of services, so when we tried tag-based approach, we start hitting various API limits. So we switched to 'classic' db approach.
It requires to setup a DynamoDB, but completely stress-free for cloud provider API.

To specify storage type, modify following key in [application.properties](src/main/resources/application.properties) or pass it as env variable on
container startup. It would use dynamo setup by default.

```
faila.instance.manager.backend=tags
```

If you are using dynamodb, following properties should be set in order to access db:

```
amazon.dynamodb.endpoint=http://dynamodb:8000
amazon.aws.accesskey=test
amazon.aws.secretkey=test
DYNAMO_RESOURCES_TABLE_NAME=resources_compose
```

##Setup SQS Queue

Faila itself is developed as cloud-native, fail-tolerant stateless service. In order to achieve this, it relies on message service
to share smallest possible chunks of work between nodes. You need to setup SQS Queue and provide following configuration on startup:

```
SQS_TASKS_QUEUE_ENDPOINT=http://sqs:9324
SQS_TASKS_QUEUE_NAME=tasks
SQS_TASKS_QUEUE_URL=http://sqs:9324/queue/tasks
```

Now you could start Faila and proceed with creating Failas.

##Creating failas

Depending on how many services you run, you could create configurations directly in storage you choose, or use provided
[OSB API](https://www.openservicebrokerapi.org/) to automate the process. You could find example request in
[create-faila-osb-resource-example.json](src/test/resources/create-faila-osb-resource-example.json):

```json
{
  "service_id": "2c8b968b-6fe1-4876-bd8b-6f14d7067134",
  "plan_id": "04431e4f-f05b-4b2b-a56f-bea4474191b4",
  "space_guid": "anystring",
  "organization_guid": "anystring",
  "parameters": {
    "type": "termination",
    "schedule": "0 0/1 * * * ?",
    "asgs": [
      "1942-b31c2e1--2018-04-20-01-23-utc--8tqkdiarc6cotir9--WebServer",
      "1942-b31c2e1--2018-04-20-01-23-utc--8tqkdiarc6cotir9--MiscJobs",
      "1942-b31c2e1--2018-04-20-01-23-utc--8tqkdiarc6cotir9--CfEvents",
      "1942-b31c2e1--2018-04-20-01-23-utc--8tqkdiarc6cotir9--CfWorker"
    ],
    "probability": 50
  }
}
```

Create `PUT` request to create service instance endpoint to create Faila:

```bash
curl -X PUT -d @create-faila-osb-resource-example.json "http://faila.url/v2/service_instances/some_id" -H "Content-Type: application/json"
```

The only available Faila type atm is `termination`. That will terminate random instance inside random AutoScalingGroup
associated with given Faila (`asgs` parameter in payload) with given `probability` at specified `schedule` (
see [Cron Trigger Tutorial](http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/crontrigger.html) on schedule cron format).
Depending on how confident you are, you could perform fully-controlled, fully-random or anything-in-between experiments.

> At our scale, we extensively use OSB REST API. Creating/updating Faila configurations is a part of deployment process in our infrastructure.

##Cloud providers

ATM it's only AWS-compatible, K8S coming soon.

#Contributors

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

Test your changes locally before creating pull request. To run tests with maven:

```bash
mvn clean test
```

## Running locally

### Prerequisites
Install OpenJDK 1.8 (MacOS)
```bash
brew tap adoptopenjdk/openjdk
brew cask install adoptopenjdk8
```

Set your `JAVA_HOME` path to OpenJDK 1.8. Optionally add this to your shell's `.rc` file
```bash
JAVA_HOME=$(/usr/libexec/java_home -v 1.8)
```

Build and package binary:
```bash
mvn clean package
```

Build docker image:
```bash
docker build -t hub.docker.com/atlassianlabs/faila:1.0-SNAPSHOT .
```

Spin-up all the stuff:
```bash
docker-compose up
```

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

#License

Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
