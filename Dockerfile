FROM openjdk:8-jre-alpine

ADD target/faila-*.jar /app.jar

CMD ["sh", "-c", "java $JAVA_OPTS -jar app.jar"]

EXPOSE 8080
