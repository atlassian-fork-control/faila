package com.atlassian.faila.failas

import com.atlassian.faila.cloud.CloudManager
import com.atlassian.faila.model.Faila
import com.timgroup.statsd.NoOpStatsDClient
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyString
import org.mockito.Mockito.mock

class AbstractFailaTest {

    class RealFailaException: RuntimeException()

    class DryRunFailaException: RuntimeException()

    class TestFaila(cloudManager: CloudManager): AbstractFaila(cloudManager, NoOpStatsDClient()) {
        override fun doRealFaila(instanceId: String) {
            throw RealFailaException()
        }

        override fun doDryRunFaila(instanceId: String) {
           throw DryRunFailaException()
        }

    }

    private val cloudManager: CloudManager = mock(CloudManager::class.java)
    private val testFaila: AbstractFaila = TestFaila(cloudManager)
    private val faila: Faila = Faila()

    @Before
    fun setUp() {
        faila.type = "termination"
    }

    @Test
    fun `faila should not be executed when zero probability`() {
        faila.probability = 0
        faila.id = "anyid"
        testFaila.perform(faila)
    }

    @Test
    fun `the default type for zero probability is termination`() {
        val faila = Faila.fromMap(hashMapOf(
            "probability" to 0,
            "asgs" to arrayListOf<String>()
        ))
        assert(faila.type.equals("termination"))
    }

    @Test(expected = RealFailaException::class)
    fun `faila should always be executed when 100 probability`() {
        faila.probability = 100
        faila.dryRun = false
        faila.asgs = setOf("anyasgs")
        `when`(cloudManager.getRandomInstanceFromAsgs(faila.asgs)).thenReturn(anyString())
        testFaila.perform(faila)
    }

    @Test(expected = DryRunFailaException::class)
    fun `dry run faila called when specified`() {
        faila.probability = 100
        faila.dryRun = true
        faila.asgs = setOf("anyasgs")
        `when`(cloudManager.getRandomInstanceFromAsgs(faila.asgs)).thenReturn(anyString())
        testFaila.perform(faila)
    }

}
