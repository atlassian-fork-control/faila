package com.atlassian.faila.servicebroker.instances.dynamo

import com.atlassian.faila.exceptions.OutdatedScheduledFailaException
import com.atlassian.faila.model.Faila
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.mock
import org.mockito.Mockito.spy

class DynamoServiceInstanceManagerTest {
    private lateinit var failaDAO: FailaDAO
    private lateinit var anyId: String
    private lateinit var nextFire: String
    private lateinit var faila: Faila
    private lateinit var anyAsg: String
    private lateinit var serviceInstanceManager: DynamoServiceInstanceManager

    @Before
    fun setUp() {
        failaDAO = mock(FailaDAO::class.java)
        anyId = System.currentTimeMillis().toString()
        nextFire = System.currentTimeMillis().toString()
        faila = Faila()
        anyAsg = System.currentTimeMillis().toString()
        serviceInstanceManager = spy(DynamoServiceInstanceManager(failaDAO))
        doReturn(faila).`when`(serviceInstanceManager).findFaila(anyId)
    }

    @Test
    fun `reset next fire returns faila with empty nextfire`() {
        faila.nextFire = nextFire
        faila.asgs = setOf(anyAsg)

        val actualFaila = serviceInstanceManager.findFailaAndResetNextFire(anyId, nextFire, setOf(anyAsg))
        assertThat(actualFaila.nextFire, `is`(""))
    }

    @Test(expected = OutdatedScheduledFailaException::class)
    fun `reset next fire throws outdated exception when not found`() {
        doReturn(null).`when`(serviceInstanceManager).findFaila(anyId)
        serviceInstanceManager.findFailaAndResetNextFire(anyId, nextFire, emptySet())
    }

    @Test(expected = OutdatedScheduledFailaException::class)
    fun `reset next fire throws outdated exception when schedule not matched`() {
        faila.nextFire = ""
        faila.asgs = setOf(anyAsg)
        serviceInstanceManager.findFailaAndResetNextFire(anyId, nextFire, setOf(anyAsg))
    }

    @Test(expected = OutdatedScheduledFailaException::class)
    fun `reset next fire throws outdated exception when asgs not matched`() {
        faila.nextFire = nextFire
        faila.asgs = emptySet()
        serviceInstanceManager.findFailaAndResetNextFire(anyId, nextFire, setOf(anyAsg))
    }

}
