package com.atlassian.faila.service

import com.atlassian.faila.cloud.CloudManager
import com.atlassian.faila.exceptions.AsgNotFoundException
import com.atlassian.faila.exceptions.EmptyAsgException
import com.atlassian.faila.exceptions.OutdatedScheduledFailaException
import com.atlassian.faila.failas.AbstractFaila
import com.atlassian.faila.failas.FailaManager
import com.atlassian.faila.model.Faila
import com.atlassian.faila.servicebroker.instances.ServiceInstanceManager
import com.timgroup.statsd.NoOpStatsDClient
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers.anyString
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.doThrow
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.spy
import org.mockito.Mockito.verify

class ExecuteServiceTest {
    private lateinit var serviceInstanceManager: ServiceInstanceManager
    private lateinit var scheduleService: ScheduleService
    private lateinit var failaManager: FailaManager
    private lateinit var candidate: Faila
    private lateinit var actualFaila: AbstractFaila
    private lateinit var anyId: String

    private lateinit var executeService: ExecuteService

    @Before
    fun setUp() {
        serviceInstanceManager = mock(ServiceInstanceManager::class.java)
        scheduleService = mock(ScheduleService::class.java)
        failaManager = spy(FailaManager(mock(CloudManager::class.java), NoOpStatsDClient()))
        anyId = System.currentTimeMillis().toString()
        candidate = Faila().apply {
            type = "termination"
            id = anyId
            asgs = emptySet()
        }
        actualFaila = mock(AbstractFaila::class.java)

        executeService = ExecuteService(serviceInstanceManager, scheduleService, failaManager)
    }

    @Test
    fun `schedules next execution after successful execution`() {
        // arrange
        `when`(serviceInstanceManager.findFailaAndResetNextFire(anyId, "", emptySet()))
            .thenReturn(candidate)
        doReturn(actualFaila).`when`(failaManager).fromType(anyString())

        // act
        executeService.executeScheduledFaila(anyId, emptySet(), "")

        // assert
        verify(scheduleService).schedule(anyId)
    }

    @Test
    fun `schedules next execution if no instances`() {
        // arrange
        `when`(serviceInstanceManager.findFailaAndResetNextFire(anyId, "", emptySet()))
            .thenReturn(candidate)
        doReturn(actualFaila).`when`(failaManager).fromType(anyString())
        doThrow(EmptyAsgException::class.java).`when`(actualFaila).perform(candidate)

        // act
        executeService.executeScheduledFaila(anyId, emptySet(), "")

        // assert
        verify(scheduleService).schedule(anyId)
    }

    @Test
    fun `does not schedule next execution if already executed`() {
        // arrange
        `when`(serviceInstanceManager.findFailaAndResetNextFire(anyId, "", emptySet()))
            .thenThrow(OutdatedScheduledFailaException::class.java)

        // act
        executeService.executeScheduledFaila(anyId, emptySet(), "")

        // assert
        verify(scheduleService, never()).schedule(anyId)
    }

    @Test
    fun `schedules next execution if asg not found`() {
        // arrange
        `when`(serviceInstanceManager.findFailaAndResetNextFire(anyId, "", emptySet()))
            .thenReturn(candidate)
        doReturn(actualFaila).`when`(failaManager).fromType(anyString())
        doThrow(AsgNotFoundException::class.java).`when`(actualFaila).perform(candidate)

        // act
        executeService.executeScheduledFaila(anyId, emptySet(), "")

        // assert
        verify(scheduleService).schedule(anyId)
    }

}
