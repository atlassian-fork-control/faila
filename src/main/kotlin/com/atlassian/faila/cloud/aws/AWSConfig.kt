package com.atlassian.faila.cloud.aws

import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.autoscaling.AmazonAutoScalingAsync
import com.amazonaws.services.autoscaling.AmazonAutoScalingAsyncClientBuilder
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2AsyncClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment

@Configuration
open class AWSConfig {
    @Autowired
    private lateinit var environment: Environment

    @Bean
    open fun currentRegion(): Region = when {
        environment.activeProfiles.contains("compose") -> Region.getRegion(Regions.DEFAULT_REGION)
        else -> Regions.getCurrentRegion()!!
    }

    @Bean
    open fun autoscalingClient(region: Region): AmazonAutoScalingAsync {
        val clientBuilder: AmazonAutoScalingAsyncClientBuilder = AmazonAutoScalingAsyncClientBuilder.standard()
        clientBuilder.withRegion(region.name)
        return clientBuilder.build()
    }

    @Bean
    open fun ec2Client(region: Region): AmazonEC2 {
        val clientBuilder: AmazonEC2AsyncClientBuilder = AmazonEC2AsyncClientBuilder.standard()
        clientBuilder.withRegion(region.name)
        return clientBuilder.build()
    }

}
