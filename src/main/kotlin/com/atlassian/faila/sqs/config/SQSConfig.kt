package com.atlassian.faila.sqs.config

import com.amazon.sqs.javamessaging.ProviderConfiguration
import com.amazon.sqs.javamessaging.SQSConnection
import com.amazon.sqs.javamessaging.SQSConnectionFactory
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import javax.jms.Connection
import javax.jms.MessageConsumer
import javax.jms.MessageListener
import javax.jms.MessageProducer
import javax.jms.Queue
import javax.jms.Session
import javax.jms.Session.CLIENT_ACKNOWLEDGE

@Configuration
open class SQSConfig {

    companion object {
        private val LOG = LoggerFactory.getLogger(SQSConfig::class.java)
    }

    @Value("\${SQS_TASKS_QUEUE_REGION:us-east-1}")
    private val sqsTasksQueueRegion: String? = null

    @Value("\${SQS_TASKS_QUEUE_NAME:}")
    private val sqsTasksQueueName: String? = null

    @Value("\${SQS_TASKS_QUEUE_ENDPOINT:}")
    private val sqsTasksQueueEndpoint: String? = null

    @Autowired
    private lateinit var environment: Environment

    @Bean
    open fun sqsClient(): AmazonSQS {
        val sqsClientBuilder = AmazonSQSClientBuilder.standard()

        if (environment.activeProfiles.contains("compose")) {
            LOG.debug("Building local SQS client with endpoint: $sqsTasksQueueEndpoint")
            val endpointConfiguration = EndpointConfiguration(sqsTasksQueueEndpoint, sqsTasksQueueRegion)
            sqsClientBuilder.withEndpointConfiguration(endpointConfiguration)
            sqsClientBuilder.withCredentials(
                AWSStaticCredentialsProvider(BasicAWSCredentials("test", "test"))
            )
        } else {
            if (!sqsTasksQueueRegion.isNullOrEmpty()) {
                LOG.debug("Setting SQS region to: $sqsTasksQueueRegion")
                sqsClientBuilder.withRegion(sqsTasksQueueRegion)
            }
        }
        return sqsClientBuilder.build()
    }

    @Bean
    open fun sqsConnection(sqsClient: AmazonSQS): SQSConnection {
        val sqsConnectionFactory = SQSConnectionFactory(ProviderConfiguration(), sqsClient)
        return sqsConnectionFactory.createConnection()
    }

    @Bean
    open fun sqsSession(sqsConnection: SQSConnection): Session = sqsConnection.createSession(false, CLIENT_ACKNOWLEDGE)

    @Bean
    open fun sqsQueue(session: Session): Queue = session.createQueue(sqsTasksQueueName)

    @Bean
    open fun messageProducer(queue: Queue, session: Session): MessageProducer = session.createProducer(queue)

    @Bean
    open fun messageConsumer(
        queue: Queue,
        session: Session,
        connection: Connection,
        messageListener: MessageListener
    ): MessageConsumer = session.createConsumer(queue).apply {
        this.messageListener = messageListener
        connection.start()
    }

}
