package com.atlassian.faila.sqs

import com.atlassian.faila.model.Faila
import com.atlassian.faila.scheduler.JobScheduler
import com.atlassian.faila.service.ExecuteService
import com.atlassian.faila.servicebroker.instances.ServiceInstanceManager
import com.atlassian.faila.sqs.FailaMessageListener.FailaMessageType.EXECUTE
import com.atlassian.faila.sqs.FailaMessageListener.FailaMessageType.SCHEDULE
import com.timgroup.statsd.StatsDClient
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.Date
import javax.jms.Message
import javax.jms.MessageListener
import javax.jms.ObjectMessage

@Component
class FailaMessageListener @Autowired constructor(
    private val jobScheduler: JobScheduler,
    private val serviceInstanceManager: ServiceInstanceManager,
    private val executeService: ExecuteService,
    private val statsDClient: StatsDClient
) : MessageListener {

    companion object {
        private val LOG = LoggerFactory.getLogger(FailaMessageListener::class.java)
        const val SERVICE_INSTANCE_ID = "osbInstanceId"
    }

    override fun onMessage(message: Message) {
        val previousMdc = MDC.getCopyOfContextMap()
        try {
            val objectMessage: ObjectMessage = message as ObjectMessage
            val type: FailaMessageType = FailaMessageType.valueOf(objectMessage.getStringProperty("type"))
            statsDClient.increment("message", "type:$type", "action:received")
            when (type) {
                SCHEDULE -> handleSchedule(objectMessage)
                EXECUTE -> handleExecute(objectMessage)
            }
            statsDClient.increment("message", "type:$type", "action:processed")
        } finally {
            MDC.setContextMap(previousMdc)
        }
    }

    private fun handleSchedule(scheduleMessage: ObjectMessage) {
        val faila: Faila = scheduleMessage.`object` as Faila

        MDC.put(SERVICE_INSTANCE_ID, faila.id)

        val nextFire: String = jobScheduler.scheduleJob(faila.schedule, faila.id, faila.asgs)
        serviceInstanceManager.setFailaNextFire(faila.id, nextFire)
        scheduleMessage.acknowledge()
        val date = Date(nextFire.toLong())
        LOG.info("Next execution scheduled to '$date'")
    }

    private fun handleExecute(executeMessage: ObjectMessage) {
        val serviceInstanceId: String = executeMessage.getStringProperty("service_instance_id")

        MDC.put(SERVICE_INSTANCE_ID, serviceInstanceId)

        val asgs: Set<String> = executeMessage.getStringProperty("asgs").split(",").toSet()
        val nextFire: String = executeMessage.getStringProperty("next_fire")
        executeService.executeScheduledFaila(serviceInstanceId, asgs, nextFire)
        executeMessage.acknowledge()
    }

    enum class FailaMessageType {
        SCHEDULE, EXECUTE
    }

}
