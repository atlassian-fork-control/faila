package com.atlassian.faila.servicebroker.instances

import com.atlassian.faila.model.Faila

abstract class ServiceInstanceManager {

    abstract fun createFaila(serviceInstanceId: String, parameters: Map<String, Any>): Faila

    abstract fun deleteFaila(serviceInstanceId: String)

    abstract fun updateFaila(serviceInstanceId: String, parameters: Map<String, Any>): Faila

    abstract fun listFailas(): Iterable<Faila>

    abstract fun findFailaAndResetNextFire(serviceInstanceId: String, nextFire: String, asgs: Set<String>): Faila

    abstract fun setFailaNextFire(serviceInstanceId: String, nextFire: String)

    abstract fun findFaila(serviceInstanceId: String): Faila?

    fun hasFaila(serviceInstanceId: String): Boolean = findFaila(serviceInstanceId) != null

    fun hasFailaWithDifferentParameters(serviceInstanceId: String, parameters: Map<String, Any>): Boolean {
        val existingFaila = findFaila(serviceInstanceId) ?: return false
        val newFaila = failaFromMap(serviceInstanceId, parameters)
        return existingFaila != newFaila
    }

    protected fun failaFromMap(serviceInstanceId: String, parameters: Map<String, Any>): Faila =
        Faila.fromMap(parameters).apply {
            id = serviceInstanceId
        }

}
