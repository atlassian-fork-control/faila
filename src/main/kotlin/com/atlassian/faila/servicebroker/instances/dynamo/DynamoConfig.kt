package com.atlassian.faila.servicebroker.instances.dynamo

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride
import org.slf4j.LoggerFactory
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment

@Configuration
@EnableDynamoDBRepositories(basePackages = ["com.atlassian.faila"], dynamoDBMapperConfigRef = "dynamoDBMapperConfig")
open class DynamoConfig {
    companion object {
        private val LOG = LoggerFactory.getLogger(DynamoConfig::class.java)
    }

    @Value("\${amazon.dynamodb.endpoint:}")
    private val amazonDynamoDBEndpoint: String? = null

    @Value("\${amazon.aws.accesskey:null}")
    private val amazonAWSAccessKey: String? = null

    @Value("\${amazon.aws.secretkey:null}")
    private val amazonAWSSecretKey: String? = null

    @Value("\${DYNAMO_RESOURCES_TABLE_NAME}")
    private val dynamoResourcesTableName: String? = null

    @Value("\${DYNAMO_RESOURCES_TABLE_REGION:}")
    private val dynamoResourcesTableRegion: String? = null

    @Autowired
    private lateinit var environment: Environment

    @Bean
    open fun amazonDynamoDB(): AmazonDynamoDB = when {
        environment.activeProfiles.contains("compose") -> localDynamoDb()
        else -> remoteDynamoDb()
    }

    @Bean
    open fun dynamoDBMapperConfig(tableNameOverrider: TableNameOverride): DynamoDBMapperConfig {
        val builder = DynamoDBMapperConfig.Builder()
        builder.tableNameOverride = tableNameOverrider
        return builder.build()
    }

    @Bean
    open fun tableNameOverrider(): TableNameOverride {
        LOG.info("Overriding table name with $dynamoResourcesTableName")
        return TableNameOverride(dynamoResourcesTableName)
    }

    private fun localDynamoDb(): AmazonDynamoDB {
        LOG.info("Building local dynamoDB client with endpoint: $amazonDynamoDBEndpoint")

        val builder = AmazonDynamoDBClientBuilder.standard()
        builder.setEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(amazonDynamoDBEndpoint,
                dynamoResourcesTableRegion))

        val creds = AWSStaticCredentialsProvider(BasicAWSCredentials(amazonAWSAccessKey, amazonAWSAccessKey))
        builder.setCredentials(creds)

        return builder.build()
    }

    private fun remoteDynamoDb(): AmazonDynamoDB {
        LOG.info("Building remote dynamoDB client")
        val clientBuilder = AmazonDynamoDBClientBuilder.standard()
        if (!dynamoResourcesTableRegion.isNullOrEmpty()) {
            LOG.info("Setting dynamoDB table region to $dynamoResourcesTableRegion")
            clientBuilder.withRegion(dynamoResourcesTableRegion)
        }
        return clientBuilder.build()
    }

}
