package com.atlassian.faila.servicebroker.config

import org.springframework.cloud.servicebroker.model.catalog.Catalog
import org.springframework.cloud.servicebroker.model.catalog.Plan
import org.springframework.cloud.servicebroker.model.catalog.ServiceDefinition
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class CatalogConfig {

    private val serviceDefinitionMetadata: Map<String, Any>
        get() {
            return mapOf("displayName" to "Faila",
                    "imageUrl" to "",
                    "longDescription" to "Faila Service",
                    "providerDisplayName" to "Atlassian")

        }

    private val planMetadata: Map<String, Any>
        get() {
            return mapOf()
        }

    @Bean
    open fun catalog(): Catalog {
        val plan = Plan.builder()
            .id("04431e4f-f05b-4b2b-a56f-bea4474191b4")
            .name("default")
            .description("This is a default Faila plan. All services are created equally.")
            .metadata(planMetadata)
            .build()
        return Catalog.builder()
            .serviceDefinitions(
                ServiceDefinition.builder()
                    .id("2c8b968b-6fe1-4876-bd8b-6f14d7067134")
                    .name("faila")
                    .description("Faila as a service")
                    .bindable(false)
                    .planUpdateable(false)
                    .plans(plan)
                    .metadata(serviceDefinitionMetadata)
                    .tags("faila", "document")
                    .build()
            )
            .build()
    }

}
