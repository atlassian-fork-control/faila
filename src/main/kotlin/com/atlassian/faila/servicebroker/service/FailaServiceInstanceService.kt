package com.atlassian.faila.servicebroker.service

import com.atlassian.faila.model.Faila
import com.atlassian.faila.service.ScheduleService
import com.atlassian.faila.servicebroker.instances.ServiceInstanceManager
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.servicebroker.exception.ServiceInstanceExistsException
import org.springframework.cloud.servicebroker.model.instance.CreateServiceInstanceRequest
import org.springframework.cloud.servicebroker.model.instance.CreateServiceInstanceResponse
import org.springframework.cloud.servicebroker.model.instance.DeleteServiceInstanceRequest
import org.springframework.cloud.servicebroker.model.instance.DeleteServiceInstanceResponse
import org.springframework.cloud.servicebroker.model.instance.GetLastServiceOperationRequest
import org.springframework.cloud.servicebroker.model.instance.GetLastServiceOperationResponse
import org.springframework.cloud.servicebroker.model.instance.UpdateServiceInstanceRequest
import org.springframework.cloud.servicebroker.model.instance.UpdateServiceInstanceResponse
import org.springframework.cloud.servicebroker.service.ServiceInstanceService
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class FailaServiceInstanceService @Autowired constructor(
    private val parametersValidator: ParametersValidator,
    private val serviceInstanceManager: ServiceInstanceManager,
    private val scheduleService: ScheduleService
) : ServiceInstanceService {

    companion object {
        private val LOG = LoggerFactory.getLogger(FailaServiceInstanceService::class.java)
    }

    @PostConstruct
    fun scheduleExistingFailas() = scheduleService.scheduleAll()

    override fun createServiceInstance(createInstanceRequest: CreateServiceInstanceRequest):
            CreateServiceInstanceResponse {

        val serviceInstanceId: String = createInstanceRequest.serviceInstanceId
        val parameters = parametersValidator.validate(createInstanceRequest.parameters)

        if (serviceInstanceManager.hasFaila(serviceInstanceId)) {
            if (serviceInstanceManager.hasFailaWithDifferentParameters(serviceInstanceId, parameters)) {
                LOG.info(
                    "Could not create service instance '$serviceInstanceId', " +
                            "it already exists and has different configuration"
                )
                throw ServiceInstanceExistsException(
                    serviceInstanceId, createInstanceRequest.serviceDefinitionId
                )
            } else {
                return CreateServiceInstanceResponse.builder().instanceExisted(true).build()
            }
        }

        val faila: Faila = serviceInstanceManager.createFaila(serviceInstanceId, parameters)
        LOG.info("Service instance created: '$faila'")

        scheduleService.schedule(faila.id)
        return CreateServiceInstanceResponse.builder().build()
    }

    override fun updateServiceInstance(updateServiceInstanceRequest: UpdateServiceInstanceRequest):
            UpdateServiceInstanceResponse {

        val serviceInstanceId: String = updateServiceInstanceRequest.serviceInstanceId
        val parameters = parametersValidator.validate(updateServiceInstanceRequest.parameters)

        val updatedFaila: Faila = serviceInstanceManager.updateFaila(serviceInstanceId, parameters)

        if (updatedFaila.nextFire == "") scheduleService.schedule(updatedFaila.id)

        return UpdateServiceInstanceResponse.builder().build()
    }

    override fun getLastOperation(getLastServiceOperationRequest: GetLastServiceOperationRequest):
            GetLastServiceOperationResponse {
        TODO()
    }

    override fun deleteServiceInstance(deleteServiceInstanceRequest: DeleteServiceInstanceRequest):
            DeleteServiceInstanceResponse {
        serviceInstanceManager.deleteFaila(deleteServiceInstanceRequest.serviceInstanceId)
        return DeleteServiceInstanceResponse.builder().build()
    }

}
