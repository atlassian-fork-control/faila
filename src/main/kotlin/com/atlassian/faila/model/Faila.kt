package com.atlassian.faila.model

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBVersionAttribute
import java.io.Serializable

@DynamoDBTable(tableName = "resources")
open class Faila : Serializable {

    @DynamoDBHashKey
    lateinit var id: String

    @DynamoDBAttribute
    lateinit var asgs: Set<String>

    @DynamoDBAttribute
    lateinit var type: String

    @DynamoDBAttribute
    lateinit var schedule: String

    @DynamoDBAttribute
    var dryRun: Boolean = DEFAULT_DRY_RUN

    @DynamoDBAttribute
    var probability: Int = DEFAULT_PROBABILITY

    @DynamoDBAttribute
    var nextFire: String = ""

    @Suppress("unused")
    @DynamoDBVersionAttribute
    var version: Long? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        when (other) {
            is Faila -> {
                if (asgs != other.asgs) return false
                if (type != other.type) return false
                if (schedule != other.schedule) return false
                if (dryRun != other.dryRun) return false
                if (probability != other.probability) return false
                return true
            }
            else -> return false
        }
    }

    override fun hashCode(): Int {
        var result = asgs.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + schedule.hashCode()
        result = 31 * result + dryRun.hashCode()
        result = 31 * result + probability.hashCode()
        return result
    }

    override fun toString(): String =
        "id: $id, asgs: $asgs, type: $type, schedule: $schedule, probability: $probability"

    companion object {
        fun fromMap(map: Map<String, Any>) = Faila().apply {
            id = map["id"].toString()
            asgs = (map["asgs"] as List<*>).filterIsInstance<String>().toSet()
            type = map["type"]?.toString() ?: DEFAULT_TYPE
            schedule = map["schedule"]?.toString() ?: DEFAULT_SCHEDULE
            dryRun = map["dryRun"] as? Boolean ?: DEFAULT_DRY_RUN
            probability = map["probability"] as? Int ?: DEFAULT_PROBABILITY
        }

        const val DEFAULT_PROBABILITY = 50
        const val DEFAULT_DRY_RUN = false
        // Only run on Jan 1 of years that divide by 65 (soonest is 2035, so... never)
        // We only expect this "never" schedule to be used with probability zero - the faila.json schema enforces this
        const val DEFAULT_SCHEDULE = "0 0 12 1 1 ? */65"
        const val DEFAULT_TYPE = "termination"
    }

}
