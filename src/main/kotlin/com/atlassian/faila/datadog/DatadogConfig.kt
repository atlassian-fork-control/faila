package com.atlassian.faila.datadog

import com.timgroup.statsd.NoOpStatsDClient
import com.timgroup.statsd.NonBlockingStatsDClient
import com.timgroup.statsd.StatsDClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class DatadogConfig {

    @Value("\${DATADOG_STATSD_HOST:}")
    private val statsdHost: String = ""

    @Value("\${DATADOG_STATSD_PORT:8125}")
    private val statsdPort: Int = 0

    @Value("\${DATADOG_STATSD_TAGS:}")
    private val statsdTags: String = ""

    @Bean
    open fun statsDClient(): StatsDClient = when {
        statsdHost.isEmpty() -> NoOpStatsDClient()
        else -> NonBlockingStatsDClient(
            "faila",
            statsdHost,
            statsdPort,
            *(statsdTags.split(",").toTypedArray())
        )
    }

}
