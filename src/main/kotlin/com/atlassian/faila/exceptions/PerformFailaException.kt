package com.atlassian.faila.exceptions

class PerformFailaException(message: String, cause: Throwable): RuntimeException(message, cause)
