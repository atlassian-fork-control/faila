package com.atlassian.faila.exceptions

import com.timgroup.statsd.StatsDClient
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.servicebroker.model.error.ErrorMessage
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class FailaExceptionHandler: ResponseEntityExceptionHandler() {
    companion object {
        val LOG = LoggerFactory.getLogger(FailaExceptionHandler::class.java)
    }

    @Autowired
    lateinit var statsDClient: StatsDClient

    @ExceptionHandler(PerformFailaException::class)
    @ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
    @ResponseBody
    fun handlePerformFailaException(ex: PerformFailaException): ErrorMessage {
        statsDClient.increment("perform.failed")
        LOG.error("Unexpected exception while performing faila", ex)
        return ErrorMessage(ex.message)
    }

    @ExceptionHandler(Exception::class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    fun handleException(ex: Exception): ErrorMessage {
        statsDClient.increment("internal_error")
        LOG.error("Unexpected exception handled by ExceptionMapper", ex)
        return ErrorMessage(ex.message)
    }

}
