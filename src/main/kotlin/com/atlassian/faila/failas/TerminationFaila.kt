package com.atlassian.faila.failas

import com.atlassian.faila.cloud.CloudManager
import com.timgroup.statsd.StatsDClient
import org.slf4j.LoggerFactory

class TerminationFaila(cloudManager: CloudManager, statsDClient: StatsDClient):
        AbstractFaila(cloudManager, statsDClient) {

    companion object {
        private val LOG = LoggerFactory.getLogger(TerminationFaila::class.java)
    }

    override fun doDryRunFaila(instanceId: String) {
        LOG.info("This is dry-run termination faila, it would terminate instance $instanceId if not dry-run")
    }

    override fun doRealFaila(instanceId: String) {
        cloudManager.terminateInstance(instanceId)
    }

}
