package com.atlassian.faila.failas

import com.atlassian.faila.cloud.CloudManager
import com.atlassian.faila.model.Faila
import com.timgroup.statsd.StatsDClient
import org.slf4j.LoggerFactory
import java.util.Random

abstract class AbstractFaila(val cloudManager: CloudManager, private val statsDClient: StatsDClient) {

    companion object {
        private val LOG = LoggerFactory.getLogger(AbstractFaila::class.java)
    }

    abstract fun doRealFaila(instanceId: String)

    abstract fun doDryRunFaila(instanceId: String)

    open fun perform(faila: Faila) {
        if (Random().nextInt(100) >= faila.probability) {
            LOG.info("Random decides not to perform faila for '${faila.id}' this time")
            statsDClient.increment("perform", "type:${faila.type}", "action:skipped")
            return
        }

        val instanceId = cloudManager.getRandomInstanceFromAsgs(faila.asgs)
        statsDClient.increment(
            "perform",
            "type:${faila.type}",
            "dryRun:${faila.dryRun}",
            "action:executed"
        )
        if (faila.dryRun) {
            doDryRunFaila(instanceId)
        } else {
            doRealFaila(instanceId)
        }
    }

}
