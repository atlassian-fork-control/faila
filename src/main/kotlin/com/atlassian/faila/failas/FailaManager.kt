package com.atlassian.faila.failas

import com.atlassian.faila.cloud.CloudManager
import com.timgroup.statsd.StatsDClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.lang.IllegalArgumentException

@Component
open class FailaManager @Autowired constructor(
    private val cloudManager: CloudManager,
    private val statsDClient: StatsDClient
) {

    open fun fromType(type: String): AbstractFaila = when (type) {
        "termination" -> TerminationFaila(cloudManager, statsDClient)
        else -> throw IllegalArgumentException("Unknown faila type '$type'")
    }

}
