package com.atlassian.faila.scheduler

import com.amazon.sqs.javamessaging.message.SQSObjectMessage
import com.atlassian.faila.sqs.FailaMessageListener.FailaMessageType.EXECUTE
import org.quartz.CronExpression
import org.quartz.Job
import org.quartz.JobBuilder.newJob
import org.quartz.JobExecutionContext
import org.quartz.Scheduler
import org.quartz.Trigger
import org.quartz.TriggerBuilder.newTrigger
import org.quartz.impl.StdSchedulerFactory
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.Date
import javax.jms.Message
import javax.jms.MessageProducer

@Component
class QuartzJobScheduler: JobScheduler {

    companion object {
        private val LOG = LoggerFactory.getLogger(QuartzJobScheduler::class.java)
    }

    private var scheduler: Scheduler

    @Autowired
    constructor (messageProducer: MessageProducer) {
        scheduler = StdSchedulerFactory().scheduler.apply {
            context["messageProducer"] = messageProducer
            start()
        }
    }

    override fun scheduleJob(schedule: String, serviceInstanceId: String, asgs: Set<String>): String {
        val jobDetail = newJob(SendExecuteMessageJob::class.java)
                .build()
        val cronExpression = CronExpression(schedule)
        val nextFireDate: Date = cronExpression.getNextValidTimeAfter(Date())
        val nextFireTime: Long = nextFireDate.time
        jobDetail.jobDataMap["service_instance_id"] = serviceInstanceId
        jobDetail.jobDataMap["asgs"] = asgs.joinToString(separator = ",")
        jobDetail.jobDataMap["next_fire"] = nextFireTime
        val jobTrigger: Trigger = newTrigger()
                .startAt(nextFireDate)
                .build()
        scheduler.scheduleJob(jobDetail, jobTrigger)
        LOG.debug("Job '${jobDetail.key}' scheduled for '$serviceInstanceId'")
        return nextFireTime.toString()
    }

    class SendExecuteMessageJob : Job {

        private val log = LoggerFactory.getLogger(SendExecuteMessageJob::class.java)

        override fun execute(context: JobExecutionContext) {
            context.scheduler.deleteJob(context.jobDetail.key)

            log.debug("Time to execute faila '${context.jobDetail.jobDataMap["service_instance_id"]}'")

            val executeMessage: Message = SQSObjectMessage("")
            executeMessage.setStringProperty("type", EXECUTE.toString())
            context.jobDetail.jobDataMap.forEach {
                executeMessage.setStringProperty(it.key, it.value.toString())
            }

            val messageProducer: MessageProducer = context.scheduler.context["messageProducer"] as MessageProducer
            messageProducer.send(executeMessage)
        }

    }

}
