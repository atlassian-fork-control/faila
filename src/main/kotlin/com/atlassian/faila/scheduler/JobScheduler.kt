package com.atlassian.faila.scheduler

interface JobScheduler {

    fun scheduleJob(schedule: String, serviceInstanceId: String, asgs: Set<String>): String

}
