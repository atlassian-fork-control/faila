package com.atlassian.faila.service

import com.atlassian.faila.exceptions.AsgNotFoundException
import com.atlassian.faila.exceptions.EmptyAsgException
import com.atlassian.faila.exceptions.OutdatedScheduledFailaException
import com.atlassian.faila.failas.FailaManager
import com.atlassian.faila.model.Faila
import com.atlassian.faila.servicebroker.instances.ServiceInstanceManager
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["faila"])
class ExecuteService @Autowired constructor(
    private val serviceInstanceManager: ServiceInstanceManager,
    private val scheduleService: ScheduleService,
    private val failaManager: FailaManager
) {

    companion object {
        private val LOG = LoggerFactory.getLogger(ExecuteService::class.java)
    }

    @RequestMapping(value = ["resources/list"], method = [GET])
    fun listResources(): Iterable<Faila> = serviceInstanceManager.listFailas()

    fun executeScheduledFaila(serviceId: String, asgs: Set<String>, nextFire: String) {
        val candidate: Faila
        try {
            candidate = serviceInstanceManager.findFailaAndResetNextFire(serviceId, nextFire, asgs)
        } catch (_: OutdatedScheduledFailaException) {
            LOG.info("Scheduled faila '$serviceId' was either already executed or updated, skipping")
            return
        }

        try {
            LOG.info("Executing scheduled faila '$serviceId' for asgs '$asgs'")
            failaManager.fromType(candidate.type).perform(candidate)
            LOG.info("Faila '$serviceId' successfully executed")
        } catch (_: AsgNotFoundException) {
            LOG.info("ASG for faila '$serviceId' was not found, skipping")
        } catch (_: EmptyAsgException) {
            LOG.info("There are no instances in asg to execute faila, skipping")
        } finally {
            scheduleService.schedule(serviceId)
        }
    }

}
